{
  "fichaTecnica": {
    "nome": "Clube Atlético Linense",
    "fundacao": "12 de junho de 1927",
    "titulos": "Série A2 (1952 e 2010) e Série A3 (1977)",
    "logoMascote": "linense.gif",
    "mascote": "Elefante"
  },
  "estadio": {
    "nome": "Gilberto Siqueira Lopes (Gilbertão)",
    "capacidade": "15.770",
    "endereco": "Rua Leopoldina, 188",
    "logo": "linense"
  },
  "destaque": {
    "nome": "Alemão - 26 anos",
    "conteudo": "Ex-companheiro de Neymar e Ganso na base do Santos, o atacante Alemão é a esperança de gols do Linense no Paulistão.",
    "logo": "alemao"
  },
  "grupo": {
    "nome": "Grupo A",
    "clubes": [
      {
        "nome": "Botafogo",
        "selecionado": false
      },
      {
        "nome": "Linense",
        "selecionado": true
      },
      {
        "nome": "Oeste",
        "selecionado": false
      },
      {
        "nome": "Santos",
        "selecionado": false
      },
      {
        "nome": "São Bento",
        "selecionado": false
      }
    ]
  },
  "proximaFase": [
    {
      "data": "31/01",
      "detalhe": "Gilbertão - Grêmio Novorizontino"
    },
    {
      "data": "03/02",
      "detalhe": "Novelli Júnior – Ituano"
    },
    {
      "data": "10/02",
      "detalhe": "Gilbertão - Ponte Preta"
    },
    {
      "data": "14/02",
      "detalhe": "Allianz Parque – Palmeiras"
    },
    {
      "data": "21/02",
      "detalhe": "Primeiro de Maio - São Bernardo"
    }
  ],
  "desempenho": [
    {
      "data": "2015",
      "conteudo": "16°"
    },
    {
      "data": "2014",
      "conteudo": "16°"
    },
    {
      "data": "2013",
      "conteudo": "9°"
    },
    {
      "data": "2012",
      "conteudo": "12°"
    },
    {
      "data": "2011",
      "conteudo": "14°"
    }
  ]
}