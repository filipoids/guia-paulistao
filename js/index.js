
var objClubes = {'clubesUm': [
	{
		nome : "Água Santa",
		img : "agua_santa.png",  
		grupo : "Grupo D",
		value :"agua-santa"
	},
	{
		nome : "Botafogo",
		img : "botafogo.png",  
		grupo : "Grupo A",
		value: "botafogo"
	},
	{
		nome : "Capivariano",
		img : "capivariano.jpg",  
		grupo : "Grupo C",
		value: "capivariano"
	},
	{
		nome : "Corinthians",
		img : "corinthians.png",  
		grupo : "Grupo D",
		value: "corinthians"
	},
	{
		nome : "Ferroviária",
		img : "ferroviaria.png",  
		grupo : "Grupo D",
		value: "ferroviaria"
	},
	{
		nome : "Ituano",
		img : "ituano.png",  
		grupo : "Grupo B",
		value: "ituano"
	}
],

'clubesDois': [
	{
		nome : "Linense",
		img : "linense.png",  
		grupo : "Grupo A",
		value :"linense"
	},
	{
		nome : "Mogi Mirim",
		img : "mogi_mirim.png",  
		grupo : "Grupo D",
		value: "mogi-mirim"
	},
	{
		nome : "Grêmio Novorizontino",
		img : "gremio_novorizontino.jpg",  
		grupo : "Grupo B",
		value: "gremio-novorizontino"
	},
	{
		nome : "Oeste",
		img : "oeste.png",  
		grupo : "Grupo A",
		value: "oeste"
	},
	{
		nome : "Grêmio Osasco Audax",
		img : "gremio_osasco_audax.png",  
		grupo : "Grupo A",
		value: "gremio-osasco-audax"
	},
	{
		nome : "Palmeiras",
		img : "palmeiras.png",  
		grupo : "Grupo B",
		value: "palmeiras"
	}
],

'clubesTres': [
	{
		nome : "Ponte Preta",
		img : "ponte_preta.png",  
		grupo : "Grupo B",
		value :"ponte-preta"
	},
	{
		nome : "Red Bull Brasil",
		img : "red_bull_brasil.png",  
		grupo : "Grupo D",
		value: "red-bull-brasil"
	},
	{
		nome : "Rio Claro",
		img : "rio_claro.png",  
		grupo : "Grupo D",
		value: "rio-claro"
	},
	{
		nome : "Santos",
		img : "santos.png",  
		grupo : "Grupo A",
		value: "santos"
	},
	{
		nome : "São Bento",
		img : "sao_bento.png",  
		grupo : "Grupo A",
		value: "sao-bento"
	},
	{
		nome : "São Bernado",
		img : "sao_bernardo.png",  
		grupo : "Grupo B",
		value: "sao-bernardo"
	}
],

'clubesQuatro': [
	{
		nome : "São Paulo",
		img : "sao_paulo.png",  
		grupo : "Grupo C",
		value :"sao-paulo"
	},
	{
		nome : "XV de Piracicaba",
		img : "xv_de_piracicaba.png",  
		grupo : "Grupo C",
		value: "xv-de-piracicaba"
	}
]
}

var Index = {

	loadUser: function(){
		  var templateUm = $('#lista-times-um').html();
		  var templateDois = $('#lista-times-dois').html();
		  var templateTres = $('#lista-times-tres').html();
		  var templateQuatro = $('#lista-times-quatro').html();

		  Mustache.parse(templateUm);   // optional, speeds up future uses
		  var rendered = Mustache.render(templateUm, objClubes);
		  $('#clubes').append(rendered);
		  Mustache.parse(templateDois); 
		  rendered = Mustache.render(templateDois, objClubes);
		  $('#clubes').append(rendered);
		   Mustache.parse(templateTres); 
		   rendered = Mustache.render(templateTres, objClubes);
		  $('#clubes').append(rendered);
		   Mustache.parse(templateQuatro); 
		   rendered = Mustache.render(templateQuatro, objClubes);
		  $('#clubes').append(rendered);

	},

	request: function(url){
		return $.get(url);
	},

	createListener: function(){
		var templateConteudo = $('#information-times').html();

		$('#clubes a').on('click', function(e){
			e.preventDefault();

			$('#clubes a').parent().parent().parent().removeClass('aberto');
			$(this).parent().parent().parent().addClass('aberto');

			$('#clubes li').removeClass('triangulo');
			$(this).parent().addClass('triangulo');
			
			Mustache.parse(templateConteudo);
			var divPai = $(this).parent().parent().parent().find('.content-info');
			
			var dataValue = $(this).data('value');
			Index.request('/data-clubes/'+dataValue+'.txt').then(function(data){
				console.log(divPai);
				var objDataParse = jQuery.parseJSON(data);

				console.log(objDataParse);

				var renderInfo = Mustache.render(templateConteudo, objDataParse);
				$(divPai).html(renderInfo);

				

			})
		})
	},

	init: function(){
		this.loadUser();

		this.createListener();
	}


}



